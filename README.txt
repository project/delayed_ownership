This module assigns a node's authorship to a user which was previously
anonymous or not logged in. As soon as the user logs in, the module checks which
nodes were created by the user and assigns them to him.

The module uses session IDs and IP addresses to track and identify users.


INSTALLATION

Steps required to use the module:
1) Unpack and install this module as every other Drupal module.
2) Configure module and user permissions.


CONFIGURATION

User configuration on admin/user/permissions:
- Anonymous users must have permission to create node types that should have 
  delayed ownership support.

Module configuration in Configuration > Content authoring > Delayed ownership (admin/config/content/delayed_ownership):
- Set the time interval from when cron will delete the created nodes from the 
  delayed_ownership database table.
- Choose content types on which delayed ownership is allowed. 
- Set text for a feedback page that appears after the anonymous user has created 
  a node that is allowed for delayed ownership. Links to user/register, 
  user/login and the main page "diskussion" are provided from the module adding 
  a smart destination.

Block configuration on admin/build/block
- A block is provided that lists the nodes that would be assigned to the 
  anonymous user after login.


USAGE

Once the module is correctly installed, you can use it the following way.
1) As an anonymous user create a node.
2) After node creation the anonymous user will redirected to a feedback page 
   that gives choices to register, login or move on without registration. 
   In the latter case, the user comes to page "diskussion" (path not yet 
   configurable).
3) After login, the user's articles will be assigned to her account.


TODOS

- Make main page link/destination configurable. 


CREDITS

The module was written by 
Stephan Grötschel <groetschel@zebralog.de> for Zebralog GmbH & Co KG 
with additions from Marco Rademacher <rademacher@zebralog.de>.
