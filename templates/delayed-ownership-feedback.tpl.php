<?php
/**
 * @file views-more.tpl.php
 * Theme the delayed ownership page.
 */
?>

<div class="do_register_text">
  <?php echo $register ?>
</div>

<div class="do_pageRight">
  <?php echo l(t('Proceed to registration'), 'user/register', array(
  'attributes' => array('class' => array('button')),
  'query' => array('destination' => $destination),
));
  ?>
</div>
<div class="do_separator"></div>

<div class="do_login_text">
  <?php echo $login ?>
</div>

<div class="do_pageRight">
  <?php echo l(t('Proceed to login'), 'user/login',
  array('attributes' => array('class' => array('button')))); ?>
    <div class="separator"></div>
</div>

<div class="do_dontregister_text">
  <?php echo $dontregister ?>
</div>

<div class="do_pageRight">
  <?php echo l(t('Proceed without registration'), $destination,
  array('attributes' => array('class' => array('button')))); ?>
</div>

